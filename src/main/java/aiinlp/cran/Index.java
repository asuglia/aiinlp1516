package aiinlp.cran;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.search.similarities.TFIDFSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockFactory;
import org.apache.lucene.store.RAMDirectory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe per indicizzare una sequenza di documenti dati in input come un unico
 * file nel formato CRAN,ovvero come un file di testo dove ogni identificativo
 * di field è individuato da una linea contenente il "." seguito da una delle
 * seguenti lettere: I, T, A, B, W. Il tag che identifica l'id del documento è
 * l'unico ad essere seguito da un token, che è appunto un intero. Tutti gli
 * altri tag sono posti su linee isolate, quindi il contenuto testuale associato
 * a ciascun campo segue nella linea successiva.
 *
 * @author asuglia
 */
public class Index {

    public enum Fields {
        ID,
        AUTHOR,
        AFFILIATION,
        TEXT,
        TITLE
    }

    FieldType textFieldNotStoredWithPosition;
    FieldType textFieldStoredWithPosition;

    /**
     * Crea i FieldType utilizzati in fase di indicizzazione.
     */
    public Index() {
        // Qui bisogna definire i due diversi FieldType che sono utilizzati
        // dalla classe
        textFieldNotStoredWithPosition = new FieldType();
        textFieldStoredWithPosition = new FieldType();

        textFieldStoredWithPosition.setTokenized(true);
        textFieldStoredWithPosition.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        textFieldStoredWithPosition.setStoreTermVectors(true);
        textFieldStoredWithPosition.setStored(true);
        textFieldStoredWithPosition.setStoreTermVectorPositions(true);


        textFieldNotStoredWithPosition.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        textFieldNotStoredWithPosition.setTokenized(true);
        textFieldNotStoredWithPosition.setStoreTermVectors(true);
        textFieldNotStoredWithPosition.setStored(false);
        textFieldNotStoredWithPosition.setStoreTermVectorPositions(true);

    }

    /**
     * Provvede a memorizzare nel documento il field dato in input associandogli
     * un nome corrispondente allo stato.
     *
     * @param doc   il documento a cui aggiungere i vari field.
     * @param text  il contenuto testuale del field che si vuole aggiungere.
     * @param state lo stato, ovvero un intero che identifica il field che si
     *              vuole inserire.
     * @throws Exception
     */
    private void storeField(Document doc, String text, Fields state) throws Exception {
        if (state.equals(Fields.ID)) {
            doc.add(new StringField("id", text, Field.Store.YES));
        } else if (state.equals(Fields.AUTHOR)) {
            doc.add(new Field("author", text, textFieldStoredWithPosition));
        } else if (state.equals(Fields.AFFILIATION)) {
            doc.add(new Field("affiliation", text, textFieldStoredWithPosition));
        } else if (state.equals(Fields.TEXT)) {
            doc.add(new Field("text", text, textFieldNotStoredWithPosition));
        } else if (state.equals(Fields.TITLE)) {
            doc.add(new Field("title", text, textFieldStoredWithPosition));
        }
    }

    /**
     * Costruisce una rappresentazione strutturata appropriata per Lucene
     * a partire dal documento contenente i documenti del CRAN
     *
     * @param cranFile il file contenente i documenti.
     * @throws Exception
     */
    public List<Document> buildDocuments(File cranFile) throws Exception {
        List<Document> docs = new ArrayList<>();

        try (BufferedReader in = new BufferedReader(new FileReader(cranFile))) {
            Document doc = null;
            StringBuffer buf = null;
            Fields state = null;
            String line;

            while ((line = in.readLine()) != null) {

                if (line.startsWith(".I")) {
                    if (buf != null && buf.length() > 0) {
                        this.storeField(doc, buf.toString(), state);
                    }

                    if (doc != null) {
                        docs.add(doc);
                    }

                    doc = new Document();
                    buf = new StringBuffer();
                    buf.append(line.replace(".I", "").trim());
                    state = Fields.ID;
                } else if (line.startsWith(".T")) {
                    if (buf != null && buf.length() > 0) {
                        this.storeField(doc, buf.toString(), state);
                    }
                    buf = new StringBuffer();
                    buf.append(line.replace(".T", "").trim());
                    state = Fields.TITLE;
                } else if (line.startsWith(".A")) {
                    if (buf != null && buf.length() > 0) {
                        this.storeField(doc, buf.toString(), state);
                    }
                    buf = new StringBuffer();
                    buf.append(line.replace(".A", "").trim());
                    state = Fields.AUTHOR;
                } else if (line.startsWith(".B")) {
                    if (buf != null && buf.length() > 0) {
                        this.storeField(doc, buf.toString(), state);
                    }
                    buf = new StringBuffer();
                    buf.append(line.replace(".B", "").trim());
                    state = Fields.AFFILIATION;
                } else if (line.startsWith(".W")) {
                    if (buf != null && buf.length() > 0) {
                        this.storeField(doc, buf.toString(), state);
                    }
                    buf = new StringBuffer();
                    buf.append(line.replace(".W", "").trim());
                    state = Fields.TEXT;
                } else {
                    if (buf != null) {
                        buf.append(line.trim());
                        buf.append("\n");
                    }
                }
            }
            if (doc != null) {
                docs.add(doc);
            }
        }
        return docs;
    }

    /**
     * Provvede ad indicizzare la lista di documenti specificata, nella cartella indexDir
     * dopo averli processato con l'analizzatore indicato.
     *
     * @param docs     lista di documenti da indicizzare
     * @param indexDir directory dell'indice da salvare
     * @param analyzer analizzatore da impiegare
     * @throws Exception
     */
    public void indexRAM(List<Document> docs, Analyzer analyzer, String similarityID) throws Exception {
        // Uses an in-memory index
        Directory indexDirWriter = new RAMDirectory();
        Similarity similarity = getSimilarityByID(similarityID);

        IndexWriterConfig indexConfig = new IndexWriterConfig(analyzer);
        if (similarity != null)
            indexConfig.setSimilarity(similarity);

        try (IndexWriter indexWriter = new IndexWriter(indexDirWriter, indexConfig)) {
            indexWriter.addDocuments(docs);
        }
    }

    public void indexFS(List<Document> docs, String indexDir, Analyzer analyzer, String similarityID) throws Exception {
        Directory indexDirWriter = FSDirectory.open(new File(indexDir).toPath());
        Similarity similarity = getSimilarityByID(similarityID);

        IndexWriterConfig indexConfig = new IndexWriterConfig(analyzer);
        if (similarity != null)
            indexConfig.setSimilarity(similarity);

        try (IndexWriter indexWriter = new IndexWriter(indexDirWriter, indexConfig)) {
            indexWriter.addDocuments(docs);
        }
    }

    private Similarity getSimilarityByID(String similarityID) {
        switch (similarityID) {
            case "LM":
                return new LMDirichletSimilarity();
            case "BM25":
                return new BM25Similarity();
            default:
                return null;
        }
    }

}
