package aiinlp.cran;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Classe che esegue le query specificate nel dataset CRAN
 *
 * @author asuglia
 */
public class Search {

    private static final int TOP = 1000;
    private static final int TOP_REL = 5;

    private static final Logger logger = Logger.getLogger(Search.class.getName());

    /**
     * Dato un file di input contenente tutte le query, esegue la ricerca sugli
     * indici una query alla volta, man mano che l'oggetto query è costruito.
     * Utilizza lo stesso analyzer utilizzato per l'indicizzazione.
     * <p>
     * Viene impiegato un meccanismo di pseudo relevance feedback impiegando
     * l'algoritmo di Rocchio. I pesi associati ai termini della query originale e a quelli
     * del centroide dei documenti rilevanti non vengono presi in considerazione.
     *
     * @param indexDir         path dell'indice costruito per il corpus
     * @param cranQueries      file contenente le query in formato CRAN
     * @param resultsFilepath  percorso del file contenente le predizioni
     * @param analyzer         analizzatore da impiegare per le query
     * @param mode             tipo di boost da impiegare per i field della query
     * @param relFeedbackField field sul quale effettuare l'operazione di relevance feedback
     * @param topFeedTerms     numero di termini da aggiungere alla query
     * @throws Exception
     */
    public void searchWithFeedback(String indexDir,
                                   String cranQueries,
                                   String resultsFilepath,
                                   Analyzer analyzer,
                                   CranQuery.QueryMode mode,
                                   String relFeedbackField,
                                   int topFeedTerms) throws Exception {
        DirectoryReader dirReader = DirectoryReader.open(new SimpleFSDirectory(new File(indexDir).toPath()));
        IndexSearcher searcher = new IndexSearcher(dirReader);

        List<CranQuery> queryList = buildQueries(cranQueries);

        try (PrintWriter writer = new PrintWriter(new FileWriter(resultsFilepath))) {
            for (CranQuery query : queryList) {
                Query newQuery = query.getQuery(mode, analyzer);
                TopDocs results = searcher.search(newQuery, TOP_REL);
                ScoreDoc[] docs = results.scoreDocs;

                Map<String, Float> centroidScores = generateRelevantCentroid(docs, searcher, relFeedbackField);

                logger.info("Relevant documents centroid computed.");

                Set<Term> queryTerms = new HashSet<>();
                newQuery.createWeight(searcher, true).extractTerms(queryTerms);

                int centroidSize = centroidScores.size();

                for (Term t : queryTerms) {
                    centroidScores.put(t.text(), centroidScores.getOrDefault(t.text(), 0f) + (1 / centroidSize));
                }

                logger.info("Added query terms to the centroid.");

                List<Term> sortedTerms =
                        centroidScores.keySet()
                                .stream()
                                .sorted((o1, o2) -> -1 * centroidScores.get(o1).compareTo(centroidScores.get(o2)))
                                .map(t -> new Term(relFeedbackField, t))
                                .limit(topFeedTerms) // limita il numero di termini da aggiungere alla nuova query
                                .collect(Collectors.toList());

                BooleanQuery.Builder transfQueryBuilder = new BooleanQuery.Builder();

                sortedTerms.stream().forEach(term -> transfQueryBuilder.add(new TermQuery(term), BooleanClause.Occur.SHOULD));

                Query trasfQuery = transfQueryBuilder.build();

                results = searcher.search(trasfQuery, TOP);
                docs = results.scoreDocs;

                for (int i = 0; i < docs.length; i++) {
                    Document currDoc = searcher.doc(docs[i].doc);
                    writer.printf("%d 1 %s %d %f %s\n",
                            query.getQueryID(),
                            currDoc.get(Index.Fields.ID.toString().toLowerCase()),
                            i,
                            docs[i].score,
                            analyzer.getClass().getName() + "_" + mode.toString());

                }

            }
        }

    }

    private Map<String, Float> generateRelevantCentroid(ScoreDoc[] relDocs,
                                                        IndexSearcher searcher,
                                                        String field) throws IOException {
        IndexReader indexReader = searcher.getIndexReader();
        Map<String, Float> termScores = new HashMap<>();
        DefaultSimilarity similarity = new DefaultSimilarity();

        for (ScoreDoc currDoc : relDocs) {
            Fields fields = indexReader.getTermVectors(currDoc.doc);
            Terms terms = fields.terms(field);
            TermsEnum termsEnum = terms.iterator();

            BytesRef term;
            PostingsEnum posting = null;

            while ((term = termsEnum.next()) != null) {
                posting = termsEnum.postings(posting);
                Term currTerm = new Term(field, term);
                int id;
                while ((id = posting.nextDoc()) != DocIdSetIterator.NO_MORE_DOCS) {
                    Float score = similarity.tf(posting.freq()) * similarity.idf(indexReader.docFreq(currTerm),
                            indexReader.numDocs());
                    String termStr = term.utf8ToString();
                    termScores.put(termStr, score + termScores.getOrDefault(termStr, 0f));

                }


            }

        }

        for (String t : termScores.keySet()) {
            termScores.put(t, termScores.get(t) / relDocs.length);
        }
        return termScores;

    }

    public void search(String indexDir, String cranQueries, String resultsPath, Analyzer analyzer,
                       CranQuery.QueryMode mode) throws Exception {
        DirectoryReader dirReader = DirectoryReader.open(new SimpleFSDirectory(new File(indexDir).toPath()));
        IndexSearcher searcher = new IndexSearcher(dirReader);

        List<CranQuery> queryList = buildQueries(cranQueries);

        try (PrintWriter writer = new PrintWriter(new FileWriter(resultsPath))) {
            for (CranQuery query : queryList) {
                Query newQuery = query.getQuery(mode, analyzer);
                TopDocs results = searcher.search(newQuery, TOP);
                ScoreDoc[] docs = results.scoreDocs;

                for (int i = 0; i < docs.length; i++) {
                    Document currDoc = searcher.doc(docs[i].doc);
                    writer.printf("%d 1 %s %d %f %s\n",
                            query.getQueryID(),
                            currDoc.get(Index.Fields.ID.toString().toLowerCase()),
                            i,
                            docs[i].score,
                            analyzer.getClass().getName() + "_" + mode.toString());

                }
            }
        }

    }


    private List<CranQuery> buildQueries(String cranQueries) throws IOException {
        List<CranQuery> queries = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(cranQueries))) {
            StringBuffer buf = new StringBuffer();
            Integer queryid = null;
            String line;

            while ((line = in.readLine()) != null) {
                if (line.startsWith(".I")) {
                    if (queryid != null) {
                        queries.add(new CranQuery(queryid, buf.toString()));
                        queryid = null;
                    }

                    buf = new StringBuffer();
                    queryid = Integer.parseInt(line.replace(".I", "").trim());
                } else if (!line.startsWith(".W")) {
                    buf.append(line.trim());
                    buf.append("\n");
                }
            }
            if (queryid != null) {
                queries.add(new CranQuery(queryid, buf.toString()));
            }
        }

        return queries;
    }
}
