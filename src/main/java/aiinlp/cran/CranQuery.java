package aiinlp.cran;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;

import java.io.IOException;

/**
 * Rappresenta una query del cran composta da un campo ID, da utilizzare nel
 * file dioutput e da una stringa composta dagli effettivi termini della query
 *
 * @author asuglia
 */
public class CranQuery {

    public static enum QueryMode {

        /**
         * Crea una query booleana con tutti i termini in OR. La query è
         * generata su tutti i field del documento.
         */
        FLAT,
        /**
         * Da importanza doppia ai termini rinvenuti nel titolo.
         */
        BOOST_TITLE
    }

    private int queryID;
    private String query;

    public CranQuery(int queryID, String query) {
        this.queryID = queryID;
        this.query = query;
    }

    public int getQueryID() {
        return queryID;
    }

    public void setQueryID(int queryID) {
        this.queryID = queryID;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String toString() {
        return queryID + ":" + query;
    }

    /**
     * restituisce un ogetto Query di Lucene, costruito come BooleanQuery. Ogni
     * termine è aggiunto in OR.
     *
     * @param mode     - FLAT o BOOST a seconda che si voglia dare importanza doppia
     *                 ai termini associati al titolo
     * @param analyzer l'analizzatore è utizzato per eseguire sui termini della
     *                 query le stesse operazioni sul testo eseguite in fase di indicizzazione
     * @return un oggetto Query di Lucene
     * @throws IOException
     */
    public Query getQuery(QueryMode mode, Analyzer analyzer) throws IOException {
        BooleanQuery.Builder booleanquery = new BooleanQuery.Builder();

        for (Index.Fields field : Index.Fields.values()) {
            String fieldName = field.toString().toLowerCase();
            if (field.equals(Index.Fields.TEXT) || field.equals(Index.Fields.TITLE))
                booleanquery.add(getQueryByField(fieldName,
                        this.query, analyzer,
                        (mode.equals(QueryMode.BOOST_TITLE) &&
                                field.equals(Index.Fields.TITLE)) ? 2 : 1), BooleanClause.Occur.SHOULD);
        }

        return booleanquery.build();
    }

    /**
     * Costruisce l'oggetto Query per il field corrente, associandogli
     * l'opportuno boost.
     *
     * @param field field corrente
     * @param query query in formato testuale da elaborare
     * @param analyzer analyzer con il quale normalizzare la query
     * @param boost eventuale boost da associare ai termini della query
     * @return oggetto Query
     * @throws IOException
     */
    private Query getQueryByField(String field, String query, Analyzer analyzer, int boost) throws IOException {
        BooleanQuery.Builder booleanquery = new BooleanQuery.Builder();
        TokenStream ts = analyzer.tokenStream(field, query);
        CharTermAttribute charTermAttribute = ts.addAttribute(CharTermAttribute.class);
        ts.reset();
        while (ts.incrementToken()) {
            TermQuery tq = new TermQuery(new Term(field, charTermAttribute.toString()));
            tq.setBoost(boost);
            booleanquery.add(tq, BooleanClause.Occur.SHOULD);
        }
        ts.close();
        return booleanquery.build();
    }

}
