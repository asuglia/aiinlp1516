package aiinlp.cran;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

import java.io.File;
import java.util.logging.Logger;

/**
 * @author asuglia
 */
public class RunCRAN {
    private static final Logger logger = Logger.getLogger(RunCRAN.class.getName());
    /**
     * Prende in input gli argomenti: 1 file da indicizzare; 2 directory degli
     * indici; 3 percorso del file contenente le query; 4 percorso del file di
     * output; 5 modo di costruzione delle query [FLAT,BOOST_TITLE] 6
     * analizzatore [WHITESPACE, STANDARD]; 6 impiegare relevance feedback
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        //Implementare una pipeline di valutazione sperimentando diverse configurazioni
        //1. utilizzare altri meccanismi di costruzione della query. Ad esempio:
        //1.1 utilizzare solo i campi "title" e "text"
        //1.2 attribuire pesi differenti
        //2. aggiungere altri analizzatori, quali ad esempio
        //2.1 EnglishAnalyzer

        if (args.length != 7) {
            logger.severe("Invalid parameters specified.");
            logger.info("USAGE: <input_dir> <index_dir> <query_file> <results_file> <query_mode> <analyzer_id> <rel_feeed>");
            System.exit(-1);
        }

        String inputFilePath = args[0],
                indexDirPath = args[1],
                queryFilePath = args[2],
                outputFilePath = args[3],
                queryMode = args[4],
                analyzerID = args[5];

        Boolean relFeedback = Boolean.parseBoolean(args[6]);

        Analyzer analyzer = null;
        switch (analyzerID) {
            case "WHITESPACE":
                analyzer = new WhitespaceAnalyzer();
                break;
            case "STANDARD":
                analyzer = new StandardAnalyzer();
                break;
            case "ENGLISH":
                analyzer = new EnglishAnalyzer();
                break;
            default:
                throw new IllegalArgumentException("Invalid analyzer ID");
        }

        File indexDir = new File(indexDirPath);
        File[] indexFiles = indexDir.listFiles();

        // Nel caso in cui fosse già presente l'indice nella cartella specificata
        // non lo sovrascrivere e procede nell'eseguire le query
        if (indexFiles != null && indexFiles.length == 0) {
            Index index = new Index();

            index.indexFS(index.buildDocuments(new File(inputFilePath)), indexDirPath, analyzer, "");
        }

        Search searcher = new Search();

        CranQuery.QueryMode mode = (queryMode.equals("FLAT"))
                ? CranQuery.QueryMode.FLAT : CranQuery.QueryMode.BOOST_TITLE;


        if (relFeedback)
            searcher.searchWithFeedback(indexDirPath, queryFilePath, outputFilePath, analyzer, mode, "text", 10);
        else
            searcher.search(indexDirPath, queryFilePath, outputFilePath, analyzer, mode);
    }

}
