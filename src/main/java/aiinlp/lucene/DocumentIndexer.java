package aiinlp.lucene;

import org.apache.lucene.analysis.it.ItalianAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;

import java.io.*;
import java.util.logging.Logger;

/**
 * Created by asuglia on 12/11/15.
 */
public class DocumentIndexer {
    private static final Logger logger = Logger.getLogger(DocumentIndexer.class.getName());
    private static final String DOCUMENT_EXTENSION = "txt";

    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            logger.severe("Invalid parameters specified.");
            logger.info("USAGE: <input_dir> <index_dir> <field_flag>");
            System.exit(-1);
        }

        String inputDirName = args[0],
                indexDirName = args[1],
                flag = args[2];

        Directory indexDir = SimpleFSDirectory.open(new File(indexDirName).toPath());
        IndexWriterConfig indexConfig = new IndexWriterConfig(new ItalianAnalyzer());

        try (IndexWriter writer = new IndexWriter(indexDir, indexConfig)) {
            File[] files = new File(inputDirName).listFiles((dir, name) -> {
                return name.endsWith(DOCUMENT_EXTENSION);
            });

            if (files.length != 0) {

                FieldType contentFieldType = getFieldType(flag);

                for (File currFile : files) {

                    try (BufferedReader reader = new BufferedReader(new FileReader(currFile.toString()))) {
                        Document d = new Document();
                        logger.info("Indexing document: " + currFile.getName());
                        d.add(new Field("content", reader, contentFieldType));
                        d.add(new StringField("identifier", currFile.getName(), Field.Store.YES));
                        writer.addDocument(d);
                    }


                }
            } else {
                logger.severe("Not able to find any documents in the specified directory.");
            }
        }

    }

    private static FieldType getFieldType(String flag) {
        FieldType fieldType = new FieldType();

        switch (flag) {
            case "t":
                fieldType.setIndexOptions(IndexOptions.DOCS);
                fieldType.setTokenized(true);
            case "tv":
                fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS);
                fieldType.setTokenized(true);
                fieldType.setStoreTermVectors(true);
            case "tvp":
                fieldType.setTokenized(true);
                fieldType.setStoreTermVectors(true);
                fieldType.setStoreTermVectorPositions(true);
                fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);

                break;
            case "tvo":
                fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
                fieldType.setTokenized(true);
                fieldType.setStoreTermVectors(true);
                fieldType.setStoreTermVectorPositions(true);
                fieldType.setStoreTermVectorOffsets(true);
                break;
        }

        return fieldType;
    }


}
