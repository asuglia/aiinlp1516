package aiinlp.lucene;

import org.apache.lucene.index.*;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.File;
import java.io.IOException;

/**
 * Created by asuglia on 12/11/15.
 */
public class MyIndexReader {

    public static void main(String[] args) throws IOException {
        String indexDirName = args[0];

        DirectoryReader indexReader = DirectoryReader.open(SimpleFSDirectory.open(new File(indexDirName).toPath()));
        System.out.println(indexReader.numDocs());
        for (int docID = 0; docID < indexReader.maxDoc(); docID++) {

            System.out.println("######## Current document " + docID + " #####################");
            Fields fields = indexReader.getTermVectors(docID);

            if (fields != null) {
                for (String field : fields) {
                    Terms terms = fields.terms(field);
                    TermsEnum termsEnum = terms.iterator();

                    BytesRef term;
                    PostingsEnum posting = null;

                    while ((term = termsEnum.next()) != null) {
                        int id;
                        posting = termsEnum.postings(posting, PostingsEnum.ALL);
                        Term currTerm = new Term(field, term);
                        while ((id = posting.nextDoc()) != DocIdSetIterator.NO_MORE_DOCS) {
                            System.out.println(String.format("term=%s - freq= %d - total_freq= %d - doc_freq = %d",
                                    term.utf8ToString(), posting.freq(),
                                    indexReader.totalTermFreq(currTerm),
                                    indexReader.docFreq(currTerm)));
                        }

                    }

                }

                System.out.println("########################################################");
                System.out.println();
                System.out.println();

            }

        }
    }
}
