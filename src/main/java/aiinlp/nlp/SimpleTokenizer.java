package aiinlp.nlp;

import com.google.common.collect.Multiset;
import com.google.common.collect.TreeMultiset;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author asuglia
 */
public class SimpleTokenizer {

    public static final String WORD_ONLY_REGEXP = "([A-Za-z]+')|[().\\[\\]\\{\\}]|[A-Za-z\\u00E0-\\u00FA0-9]+";
    private static final Logger logger = Logger.getLogger(SimpleTokenizer.class.getName());

    /**
     * Divides into tokens the text contained in a file whose name is specified as the first argument.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Specify at least one argument
        if (args.length != 1) {
            logger.log(Level.SEVERE, "Usage: specify file path as an argument");
            return;
        }

        for (int i = 1; i <= 3; i++) {
            String file = "data/en_" + i;

            final Multiset<String> tokenSet = TreeMultiset.create(),
                    stemSet = TreeMultiset.create();

            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                Pattern p = Pattern.compile(WORD_ONLY_REGEXP);
                String line;

                GeneralStemmer stemmer = new GeneralStemmer("EN");

                while ((line = reader.readLine()) != null) {
                    Matcher m = p.matcher(line);
                    while (m.find()) {
                        String token = m.group(), stem = stemmer.stem(token);
                        tokenSet.add(token);
                        stemSet.add(stem);
                    }
                }


                System.out.println("##### TOKENS ######");
                for (String token : tokenSet.elementSet()) {
                    System.out.println(String.format("%s\t%d", token, tokenSet.count(token)));
                }

                System.out.println("####### STEMS #######");

                for (String token : stemSet.elementSet()) {
                    System.out.println(String.format("%s\t%d", token, stemSet.count(token)));
                }

                /*
                List<String> sortTokens = tokenSet.stream().sorted(new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return -1 * new Integer(tokenSet.count(o1)).compareTo(tokenSet.count(o2));
                    }
                }).collect(Collectors.toList());

                List<String> sortStem = stemSet.stream().sorted(new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return -1 * new Integer(stemSet.count(o1)).compareTo(stemSet.count(o2));
                    }
                }).collect(Collectors.toList());

                System.out.println("### COUNT SORT ######");
                for (String token : sortTokens) {
                    System.out.println(String.format("%s\t%d", token, tokenSet.count(token)));
                }

                System.out.println("######################");

                System.out.println("########## COUNT STEM SORT #########");
                for (String token : sortStem) {
                    System.out.println(String.format("%s\t%d", token, stemSet.count(token)));
                }

                System.out.println("######################");


                PriorityQueue<String> priorTokens = new PriorityQueue<>(new Comparator() {
                    @Override
                    public int compare(Object o1, Object o2) {
                        return -1 * new Integer(tokenSet.count(o1)).compareTo(tokenSet.count(o2));
                    }
                });

                int k = 3;

                for(String token : tokenSet.elementSet()) {
                    if (priorTokens.size() == k) {
                        if(tokenSet.count(priorTokens.peek()) < tokenSet.count(token)) {
                            priorTokens.poll();
                            priorTokens.add(token);
                        }
                    } else {

                        priorTokens.add(token);
                    }

                }

                while(!priorTokens.isEmpty()) {
                    System.out.println(priorTokens.poll());
                }

                */
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
    }

}

class PairComp implements Comparator<String> {
    private Multiset<String> set;

    public PairComp(Multiset<String> set) {
        this.set = set;
    }

    public int compare(String o1, String o2) {
        return -1 * new Integer(set.count(o1)).compareTo(set.count(o2));
    }

}

class Pair {
    public String key;
    public Integer count;

    public Pair(String key, Integer count) {
        this.key = key;
        this.count = count;
    }
}
