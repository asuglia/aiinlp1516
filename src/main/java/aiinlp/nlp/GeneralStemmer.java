package aiinlp.nlp;

import org.tartarus.snowball.SnowballProgram;
import org.tartarus.snowball.ext.EnglishStemmer;
import org.tartarus.snowball.ext.ItalianStemmer;

/**
 * Created by asuglia on 21/10/15.
 */
public class GeneralStemmer {
    private SnowballProgram stemmer;

    public GeneralStemmer(String lang) {
        switch(lang) {
            case "EN":
                stemmer = new EnglishStemmer();
                break;
            case "IT":
                stemmer = new ItalianStemmer();
                break;
            default:
                throw new IllegalArgumentException("No valid language " + lang);
        }
    }

    public String stem(String token) {
        stemmer.setCurrent(token);
        stemmer.stem();
        return stemmer.getCurrent();
    }
}
