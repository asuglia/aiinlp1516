package aiinlp.nlp;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;
import opennlp.tools.util.model.BaseModel;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by asuglia on 16/11/15.
 */
public class NLPPipeline {
    private static void stanfordPipeline(String filename, String propFilename) throws IOException {
        Properties props = new Properties();
        props.load(new FileReader(propFilename));

        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line = null;
            StringBuilder builder = new StringBuilder();

            while ((line = reader.readLine()) != null) {
                builder.append(line).append("\n");
            }

            String text = builder.toString();

            StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

            Annotation document = new Annotation(text);

            pipeline.annotate(document);

            List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);

            System.out.println("TOKEN\tPOS\tNER");
            for (CoreMap sentence : sentences) {
                List<CoreLabel> tokens = sentence.get(CoreAnnotations.TokensAnnotation.class);

                for (CoreLabel token : tokens) {
                    System.out.println(String.format("%s\t%s\t%s",
                            token.get(CoreAnnotations.TextAnnotation.class),
                            token.get(CoreAnnotations.PartOfSpeechAnnotation.class),
                            token.get(CoreAnnotations.NamedEntityTagAnnotation.class)));
                }

                System.out.println("Parsed tree");
                Tree tree = sentence.get(TreeCoreAnnotations.TreeAnnotation.class);
                System.out.println(tree.toString());
                System.out.println("Dependency graph");
                SemanticGraph graph = sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
                graph.prettyPrint();

            }

        }

    }

    private static void opennlpPipeline(String filename, String... modelsFilenames) throws IOException {
        List<BaseModel> models = new ArrayList<>();
        int i = 0;
        for (String modelFilename : modelsFilenames) {

            InputStream modelInput = new FileInputStream(modelFilename);
            switch (i) {
                case 0:
                    models.add(new TokenizerModel(modelInput));
                    break;
                case 1:
                    models.add(new POSModel(modelInput));
                    break;
                case 2:
                    models.add(new TokenNameFinderModel(modelInput));
                    break;

            }

            i++;

        }

        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line = null;
            StringBuilder builder = new StringBuilder();

            while ((line = reader.readLine()) != null) {
                builder.append(line).append("\n");
            }

            String text = builder.toString();

            TokenizerModel tokenizerModel = (TokenizerModel) models.get(0);
            TokenizerME tokenizer = new TokenizerME(tokenizerModel);

            String[] tokens = tokenizer.tokenize(text);

            POSModel posModel = (POSModel) models.get(1);
            POSTaggerME posTagger = new POSTaggerME(posModel);

            String[] tags = posTagger.tag(tokens);

            TokenNameFinderModel nerModel = (TokenNameFinderModel) models.get(2);
            NameFinderME nameFinder = new NameFinderME(nerModel);

            Span[] names = nameFinder.find(tokens);

            for (i = 0; i < tokens.length; i++) {
                System.out.println(String.format("%s\t%s", tokens[i], tags[i]));
            }

            System.out.println("NER elements: ");
            for (i = 0; i < names.length; i++) {
                System.out.println(names[i]);
            }

        }


    }


    public static void main(String[] args) throws IOException {
        // gerbill -  entity linking platform

        // stanfordPipeline(args[0], args[1]);
        // data/en_1.txt data/stanford.properties
        opennlpPipeline(args[0], args[1], args[2], args[3]);
        // data/en_1.txt data/open_nlp/en-token.bin data/open_nlp/en-pos-maxent.bin
        // data/open_nlp/en-ner-organization.bin
    }
}
